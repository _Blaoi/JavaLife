public class Life {
	private static char empty_cell = '.';
	private static char busy_cell = '*';
	
	public static void main(String[] args){
		Character[][] world = init_world(30, 15);
		Character[][] tmp_world = null;
		/* Bar */
		world[5][5] = busy_cell;
		world[5][6] = busy_cell;
		world[5][7] = busy_cell;
		/* Square */
		world[12][8] = busy_cell;
		world[12][9] = busy_cell;
		world[13][8] = busy_cell;
		world[13][9] = busy_cell;
		while(true){
			try{
				print_world(world);
				System.out.println("___________________________________________");
				tmp_world = copy_world(world);
				for(int i = 1;i < world.length - 1;i++){
					for(int j = 1;j < world[i].length - 1;j++){
						int neighbour = get_neighbours(world,i,j);
						if(world[i][j] == busy_cell){ //if the cell is alive
							if(neighbour != 2 && neighbour != 3){
								tmp_world[i][j] = empty_cell;
							}
						}else{ //if the cell is dead
							if(neighbour == 3){
								tmp_world[i][j] = busy_cell;
							}
						}
					}
				}
				world = copy_world(tmp_world);
				Thread.sleep(1000);
				//Runtime.getRuntime().exec("clear"); /* Change according OS. */
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
		}
	}
	
	private static Character[][] copy_world(Character[][] world) {
		Character[][] tmp = new Character[world.length][world[0].length];
		for(int i = 0;i < world.length;i++){
			for(int j = 0;j < world[0].length;j++){
				tmp[i][j] = world[i][j];
			}
		}
		return tmp;
	}

	private static int get_neighbours(Character[][] world, int h, int w) {
		int neighbour = 0;
		if(world[h - 1][w] == busy_cell){
			neighbour += 1;
		}
		if(world[h + 1][w] == busy_cell){
			neighbour += 1;
		}
		if(world[h][w - 1] == busy_cell){
			neighbour += 1;
		}
		if(world[h][w + 1] == busy_cell){
			neighbour += 1;
		}
		if(world[h - 1][w - 1] == busy_cell){
			neighbour += 1;
		}
		if(world[h + 1][w + 1] == busy_cell){
			neighbour += 1;
		}
		if(world[h + 1][w - 1] == busy_cell){
			neighbour += 1;
		}
		if(world[h - 1][w + 1] == busy_cell){
			neighbour += 1;
		}
		return neighbour;
	}

	private static void print_world(Character[][] world) {
		for(int i = 0;i < world.length;i++){
			for(int j = 0;j < world[i].length;j++){
				System.out.print(world[i][j]);
			}
			System.out.println();
		}
	}

	public static Character[][] init_world(int width, int height){
		Character[][] tmp = new Character[height][width];
		for(int i = 0;i < tmp.length;i++){
			for(int j = 0;j < tmp[i].length;j++){
				tmp[i][j] = empty_cell;
			}
		}
		return tmp;
	}
}
